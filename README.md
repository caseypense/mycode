# mycode (Project Title)

Starting to learn how to do network automation with Python!
I want to learn how to version control with git as well.
## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites
"How to Install the Language: "

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* Casey Pense - https://www.linkedin.com/in/casey-pense/
